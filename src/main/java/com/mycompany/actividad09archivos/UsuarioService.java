package com.mycompany.actividad09archivos;

import java.io.IOException;

public class UsuarioService {

    private final static int USER_MIN = 4;
    private final static int USER_MAX = 8;
    private final static int PASS_MIN = 6;
    private final static int PASS_MAX = 20;

    public void guardar(Usuario usuario) throws IOException {

        String user = usuario.getUser();
        String pass = usuario.getPass();

        if (user.length() < USER_MIN || user.length() > USER_MAX || !user.matches("^[^|]*$")) {
            throw new IllegalArgumentException("El nombre de usuario debe contener entre 4 y 8 caracteres, y no contener el caracter pipe.");
        }
        if (pass.length() < PASS_MIN || pass.length() > PASS_MAX || !pass.matches("^[^|]*[0-9][^|]*$")) {
            throw new IllegalArgumentException("El password debe contener entre 6 y 20 caracteres, con al menos 1 dígito, y no debe contener un pipe.");
        }
        UsuarioRepository repositorio = new UsuarioRepository();
        repositorio.guardar(usuario);
    }
}
