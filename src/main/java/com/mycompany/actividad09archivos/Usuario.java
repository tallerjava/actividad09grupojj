package com.mycompany.actividad09archivos;

public class Usuario {
    private String user;
    private String pass;
    
    Usuario(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }
    
    public String getUser() {
        return user;
    }
    
    public String getPass() {
        return pass;
    }
    
    public void setUser(String usuario) {
        user = usuario;
    }
    
    public void setPass(String password) {
        pass = password;
    }
}
