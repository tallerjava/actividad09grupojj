package com.mycompany.actividad09archivos;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        try {
            if (args == null || args.length <= 1) {
                throw new IllegalArgumentException("Faltan parametros requeridos: Usuario Password.");
            } else {
                Usuario usuario = new Usuario(args[0], args[1]);
                UsuarioService serviciosDeUsuario = new UsuarioService();
                serviciosDeUsuario.guardar(usuario);
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
