package com.mycompany.actividad09archivos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class UsuarioRepository {

    public void guardar(Usuario usuario) throws IOException {

        File archivo = new File("UsuarioPassword.txt");
        FileWriter fileWriter;
        if (archivo.exists()) {
            FileReader fileReader = new FileReader(archivo);
            try (BufferedReader leer = new BufferedReader(fileReader)) {
                String linea;
                while ((linea = leer.readLine()) != null) {
                    if (linea.split("\\|")[0].equals(usuario.getUser())) {
                        throw new IllegalArgumentException("El nombre de usuario ya está en uso.");
                    }
                }
            }
            fileWriter = new FileWriter(archivo, true);
            try (BufferedWriter escribir = new BufferedWriter(fileWriter)) {
                escribir.append("\r\n");
                escribir.write(usuario.getUser() + "|" + usuario.getPass());
            }
        } else {
            fileWriter = new FileWriter(archivo);
            try (BufferedWriter escribir = new BufferedWriter(fileWriter)) {
                escribir.write(usuario.getUser() + "|" + usuario.getPass());
            }
        }
    }
}
