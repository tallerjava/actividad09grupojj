package com.mycompany.actividad09archivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class UsuarioServicesTest {

    @Before
    public void setUp() {
        File archivo = new File("UsuarioPassword.txt");
        if (archivo.exists()) {
            archivo.delete();
        }
    }

    @AfterClass
    public static void tearDownClass() {
        File archivo = new File("UsuarioPassword.txt");
        if (archivo.exists()) {
            archivo.delete();
        }
    }

    @Test
    public void guardar_archivoNoExiste_seGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("User", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
/*
    @Test
    public void guardar_usuarioNoExisteEnElArchivoExistente_seGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("User1", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
        usuario.setUser("User2");
        usuario.setPass("Pas22s");
        serviciosDeUsuario.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User1|Pas11s";
            assertEquals(resultadoEsperado, resultadoObtenido);
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User2|Pas22s";
            assertEquals(resultadoEsperado, resultadoObtenido);
            buffer.close();
        }
    }
*/
    @Test
    public void guardar_usuarioYaExisteEnElArchivo_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("User", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
        usuario.setUser("User");
        usuario.setPass("Pas19s");
       /* serviciosDeUsuario.guardar(usuario);*/
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            while ((resultadoObtenido = buffer.readLine()) != null) {
                resultadoEsperado = "User|Pas11s";
                assertEquals(resultadoEsperado, resultadoObtenido);
            }
            buffer.close();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_usuarioMenorQueElMinimoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Tom", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
    }

    @Test
    public void guardar_usuarioIgualQueElMinimoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Hola", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Hola|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardar_usuarioIgualQueElMaximoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {

        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaaaaa", "Pas11s");
        serviciosDeUsuario.guardar(usuario);

        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Holaaaaa|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_usuarioMayorQueElMaximoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaaaaaa", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_usuarioConPipe_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Hoo|a", "Pas11s");
        serviciosDeUsuario.guardar(usuario);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_passMenorQueElMinimoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaa", "Pass1");
        serviciosDeUsuario.guardar(usuario);
    }

    @Test
    public void guardar_passIgualQueElMinimoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaa", "Pass12");
        serviciosDeUsuario.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Holaa|Pass12";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardar_passIgualQueElMaximoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaa", "Pass012346789zzzzzzz");
        serviciosDeUsuario.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Holaa|Pass012346789zzzzzzz";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_passMayorQueElMaximoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaa", "Pass012346789zzzzzzzz");
        serviciosDeUsuario.guardar(usuario);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_passConPipe_noSeGuardaEnElArchivo() throws Exception {
        UsuarioService serviciosDeUsuario = new UsuarioService();
        Usuario usuario = new Usuario("Holaa", "Pas|o11s");
        serviciosDeUsuario.guardar(usuario);
    }
}
