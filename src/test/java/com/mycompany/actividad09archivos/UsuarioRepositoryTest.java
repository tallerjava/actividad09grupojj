
package com.mycompany.actividad09archivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class UsuarioRepositoryTest {
    
    @Before
    public void setUp() {
        File archivo = new File("UsuarioPassword.txt");
        if (archivo.exists()) {
            archivo.delete();
        }
    }

    @AfterClass
    public static void tearDownClass() {
        File archivo = new File("UsuarioPassword.txt");
        if (archivo.exists()) {
            archivo.delete();
        }
    }

    @Test
    public void guardar_archivoNoExiste_seGuardaEnElArchivo() throws Exception {
        UsuarioRepository repositorio = new UsuarioRepository();
        Usuario usuario = new Usuario("User", "Pas11s");
        repositorio.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardar_usuarioNoExisteEnElArchivoExistente_seGuardaEnElArchivo() throws Exception {
        UsuarioRepository repositorio = new UsuarioRepository();
        Usuario usuario = new Usuario("User1", "Pas11s");
        repositorio.guardar(usuario);
        usuario.setUser("User2");
        usuario.setPass("Pas22s");
        repositorio.guardar(usuario);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User1|Pas11s";
            assertEquals(resultadoEsperado, resultadoObtenido);
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User2|Pas22s";
            assertEquals(resultadoEsperado, resultadoObtenido);
            buffer.close();
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void guardar_usuarioYaExisteEnElArchivo_noSeGuardaEnElArchivo() throws Exception {
        UsuarioRepository repositorio = new UsuarioRepository();
        Usuario usuario = new Usuario("User", "Pas11s");
        repositorio.guardar(usuario);
        usuario.setUser("User");
        usuario.setPass("Pas19s");
        repositorio.guardar(usuario);     
    }
}
