package com.mycompany.actividad09archivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;

public class AppTest {

    @Before
    public void setUp() {
        File archivo = new File("UsuarioPassword.txt");
        if (archivo.exists()) {
            archivo.delete();
        }
    }

    @AfterClass
    public static void tearDownClass() {
        File archivo = new File("UsuarioPassword.txt");
        if (archivo.exists()) {
            archivo.delete();
        }
    }

    @Test
    public void main_archivoNoExiste_seGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"User", "Pas11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void main_usuarioNoExisteEnElArchivoExistente_seGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"User1", "Pas11s"};
        App.main(args);
        args = new String[]{"User2", "Pas22s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User1|Pas11s";
            assertEquals(resultadoEsperado, resultadoObtenido);
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "User2|Pas22s";
            assertEquals(resultadoEsperado, resultadoObtenido);
            buffer.close();
        }
    }

    @Test
    public void main_usuarioYaExisteEnElArchivo_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"User", "Pas11s"};
        App.main(args);
        args = new String[]{"User", "Pas19s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            while ((resultadoObtenido = buffer.readLine()) != null) {
                resultadoEsperado = "User|Pas11s";
                assertEquals(resultadoEsperado, resultadoObtenido);
            }
            buffer.close();
        }
    }

    @Test
    public void main_usuarioMenorQueElMinimoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Tom", "Pas11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_usuarioIgualQueElMinimoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Hola", "Pas11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Hola|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void main_usuarioIgualQueElMaximoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaaaaa", "Pas11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Holaaaaa|Pas11s";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void main_usuarioMayorQueElMaximoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaaaaaa", "Pas11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_usuarioConPipe_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Hoo|a", "Pas11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_passMenorQueElMinimoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa", "Pass1"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_passIgualQueElMinimoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa", "Pass12"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Holaa|Pass12";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void main_passIgualQueElMaximoDeCaracteresPermitido_seGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa", "Pass012346789zzzzzzz"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        FileReader file = new FileReader(archivo);
        String resultadoObtenido;
        String resultadoEsperado;
        try (BufferedReader buffer = new BufferedReader(file)) {
            resultadoObtenido = buffer.readLine();
            resultadoEsperado = "Holaa|Pass012346789zzzzzzz";
            buffer.close();
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void main_passMayorQueElMaximoDeCaracteresPermitido_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa", "Pass012346789zzzzzzzz"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_passConPipe_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa", "Pas|o11s"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_passSinNumero_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa", "Pozooooooo"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }

    @Test
    public void main_sinParametros_noSeGuardaEnElArchivo() throws Exception {
        String[] args = null;
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }
/*
    @Test
    public void main_unSoloParametros_noSeGuardaEnElArchivo() throws Exception {
        String[] args = new String[]{"Holaa"};
        App.main(args);
        File archivo = new File("UsuarioPassword.txt");
        assertFalse(archivo.exists());
    }*/
}
